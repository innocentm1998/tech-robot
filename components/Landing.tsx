import React, { useState, useEffect, useRef } from 'react';

export default function Landing() {
  const [gridPosition, setGridPosition] = useState([0, 0]);
  const [previousGridPosition, setPreviousGridPosition] = useState([0, 0]);
  const [direction, setDirection] = useState('NORTH');
  const [previousDirection, setPreviousDirection] = useState('NORTH');
  const [outputLog, addToOutput] = useState('');
  const [instruction, setInstruction] = useState('');
  const [allInstructions, addInstruction] = useState('');
  const [currentPosition, setCurrentPosition] = useState('0, 0, NORTH');

  const logPositionError = () => {
    console.log('logging position error');
    addToOutput(
      outputLog +
        ' Error: This gridPosition ' +
        gridPosition +
        ' or direction ' +
        direction +
        ' would be off the grid. Position and direction values have been reset to the last gridPosition: ' +
        currentPosition
    );
    setDirection(previousDirection);
    setGridPosition(previousGridPosition);
  };

  const logInstructionError = () => {
    console.log('logging instuction error');
    addToOutput(outputLog + ' invalid instruction provided: ' + instruction);
  };

  const inGrid = (gridPos: number[]) => {
    // fix this then  all good
    let inBool: boolean = false;
    if (
      gridPos[0] >= 0 &&
      gridPos[0] <= 5 &&
      gridPos[1] >= 0 &&
      gridPos[1] <= 5
    ) {
      inBool = true;
    } else {
      inBool = false;
    }
    console.log('inGrid output ', inBool + 'gridposition', gridPosition);

    return inBool;
  };

  const isValidDirection = (dir: string) => {
    let dirBool = false;
    if (
      dir === 'NORTH' ||
      dir === 'EAST' ||
      dir === 'SOUTH' ||
      dir === 'WEST'
    ) {
      dirBool = true;
    }
    console.log('dirBool: ', dirBool);
    return dirBool;
  };

  const stringifyPosition = () => {
    console.log('stringifying');
    setCurrentPosition(gridPosition.toString() + ', ' + direction);
  };

  useEffect(() => {
    stringifyPosition();
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    addInstruction(allInstructions + '\n' + instruction);

    let instructionStr: string = instruction.toUpperCase();
    let cleanInst: string[] = instructionStr
      .split(/[, ]+/)
      .filter((element) => element);

    console.log('cleanInst', cleanInst);

    let arrLength: number = cleanInst.length;

    switch (arrLength) {
      // on MOVE
      case 1:
        if (cleanInst[0] === 'MOVE') {
          console.log('move command');
          if (direction === 'NORTH') {
            setPreviousGridPosition(gridPosition);
            setGridPosition([gridPosition[0], (gridPosition[1] += 1)]);
          }
          if (direction === 'SOUTH') {
            setPreviousGridPosition(gridPosition);
            setGridPosition([gridPosition[0], (gridPosition[1] -= 1)]);
          }
          if (direction === 'EAST') {
            setPreviousGridPosition(gridPosition);
            setGridPosition([(gridPosition[0] += 1), gridPosition[1]]);
          }
          if (direction === 'WEST') {
            setPreviousGridPosition(gridPosition);
            setGridPosition([(gridPosition[0] -= 1), gridPosition[1]]);
          }
        } else if (cleanInst[0] === 'LEFT') {
          setPreviousGridPosition(gridPosition);
          setGridPosition([(gridPosition[0] -= 1), gridPosition[1]]);
        } else if (cleanInst[0] === 'RIGHT') {
          setPreviousGridPosition(gridPosition);
          setGridPosition([(gridPosition[0] += 1), gridPosition[1]]);
        } else if (cleanInst[0] === 'REPORT') {
          console.log('instruction to report');
          addToOutput(outputLog + '\n' + currentPosition);
        }

        if (inGrid(gridPosition)) {
          stringifyPosition();
          console.log('case 1 gridPosition', gridPosition);
          break;
        } else {
          logPositionError();
          break;
        }

      // place with no direction involved
      case 3:
        if (cleanInst[0] === 'PLACE') {
          setPreviousGridPosition(gridPosition);
          setGridPosition([parseInt(cleanInst[1]), parseInt(cleanInst[2])]);
          console.log('case 3 gridPosition', gridPosition);
          if (inGrid(gridPosition)) {
            stringifyPosition();
            break;
          } else {
            logPositionError();
            break;
          }
        } else {
          logInstructionError();
        }

      // place with direction i.e. N, S, E, W
      case 4:
        if (cleanInst[0] === 'PLACE') {
          setPreviousDirection(direction);
          setPreviousGridPosition(gridPosition);
          setGridPosition(() => [
            parseInt(cleanInst[1]),
            parseInt(cleanInst[2]),
          ]);
          setDirection(cleanInst[3]);
          console.log('case 4 gridPosition', gridPosition);
          if (inGrid(gridPosition) && isValidDirection(cleanInst[3])) {
            stringifyPosition();
            break;
          } else {
            logPositionError();
            break;
          }
        } else {
          logInstructionError();
        }

      default:
        console.log(
          'default cause instruction error gridposition',
          gridPosition
        );
        logInstructionError();
        break;
    }
  };

  const handleChange = (e) => {
    setInstruction(e.target.value);
  };

  return (
    <div className='relative'>
      <div className='relative mx-auto max-w-7xl w-full h-auto pt-16 pb-20 text-center lg:py-24'>
        <form onSubmit={handleSubmit}>
          <div>
            <h1 className='text-4xl tracking-tight font-extrabold text-purple-500 sm:text-4xl mb-2'>
              Tech Robot
            </h1>
          </div>
          <div>
            <label className='mt-3 max-w-md mx-auto text-xl text-black sm:text-xl md:mt-5 md:max-w-3xl'>
              Input directions below
            </label>
          </div>
          <input
            className='border-solid border-2 border-purple-600 focus:border-purple-600 mt-2'
            name='instruction'
            value={instruction}
            onChange={handleChange}
          />
          <div>
            <h1 className='text-lg tracking-tight font-extrabold text-black-500 sm:text-lg mt-4'>
              Instuctions Log
            </h1>
            <p className='mt-4'>{allInstructions}</p>
          </div>
          <div>
            <h1 className='text-lg tracking-tight font-extrabold text-black-500 sm:text-lg mt-8'>
              Output Log
            </h1>
            <p className='mt-4'>{outputLog}</p>
          </div>
        </form>
      </div>
    </div>
  );
}
