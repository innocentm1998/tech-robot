import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import HomeIntro from '../components/Landing';

const Home: NextPage = () => {
  return (
    <div>
      <HomeIntro />
    </div>
  );
};

export default Home;
